import Exception from "@/Core/System/Exception";

export default class NotImplementedException extends Exception{
    constructor(message:string="The method or operation is not implemented"){
        super(message);
    }
}