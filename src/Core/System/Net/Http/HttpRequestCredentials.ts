export enum HttpRequestCredentials {
    Omit="omit",
    SameOrigin="same-origin",
    Include="include"
}