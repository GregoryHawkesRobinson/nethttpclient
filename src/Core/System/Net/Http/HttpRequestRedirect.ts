export enum HttpRequestRedirect {
    Follow="follow",
    Error="error",
    Manual="manual"
}