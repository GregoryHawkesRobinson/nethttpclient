export enum HttpRequestMode {
    Navigate="navigate",
    SameOrigin="same-origin",
    NoCors="no-cores",
    Cors="cors"
}