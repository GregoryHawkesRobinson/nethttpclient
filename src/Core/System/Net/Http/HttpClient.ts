import {HttpHeaders} from "@/Core/System/Net/Http/Headers/HttpHeaders";
import {IHttpMiddleware} from "@/Core/System/Net/Http/IHttpMiddleware";
import {HttpRequestMessage} from "@/Core/System/Net/Http/HttpRequestMessage";
import {HttpResponseMessage} from "@/Core/System/Net/Http/HttpResponseMessage";
import {HttpHeader} from "@/Core/System/Net/Http/Headers/HttpHeader";
import {HttpMethods} from "@/Core/System/Net/Http/HttpMethods";
import {HttpContent} from "@/Core/System/Net/Http/HttpContent";
import Debug from "@/Core/System/Diagnostics/Debug";

export interface IHttpClientInit {
    baseAddress?: string,
    timeOut?: number
}

export default class HttpClient {

    constructor(param?: IHttpClientInit) {
        if (Object.isNullOrUndefined(param)) {
            return;
        }

        if (!Object.isNullOrUndefined(param!.baseAddress)) {
            this.baseAddress = param!.baseAddress!;
        }

        if (!Object.isNullOrUndefined(param!.timeOut)) {
            this.timeOut = param!.timeOut!;
        }
    }

    //#region Public properties

    //Gets or sets the base url for all requests made
    public baseAddress: string = String.empty;

    //Gets or sets the headers that should be send with each request
    public defaultRequestHeaders: HttpHeaders = new HttpHeaders();

    //Gets or sets the number of milliseconds before the request times out
    public timeOut: number = 100000;

    //#endregion

    //#region Static properties

    public static middleware: Map<string, IHttpMiddleware> = new Map<string, IHttpMiddleware>();

    //#endregion

    //#region Static methods

    public static useMiddleware(name: string, middleware: IHttpMiddleware): void {
        let item: IHttpMiddleware | undefined = this.middleware.get(name);
        if (Object.isNullOrUndefined(item)) {
            this.middleware.set(name, middleware);
        }
    }

    //#endregion

    //#region Public methods

    public deleteAsync = async (requestUrl: string): Promise<HttpResponseMessage> => {

        //Create the request object
        let request: HttpRequestMessage = new HttpRequestMessage();
        request.method = HttpMethods.Delete;
        request.requestUri = requestUrl;

        return await this.sendAsync(request);
    };

    public getAsync = async (requestUrl: string): Promise<HttpResponseMessage> => {

        //Create the request object
        let request: HttpRequestMessage = new HttpRequestMessage();
        request.method = HttpMethods.Get;
        request.requestUri = requestUrl;

        return await this.sendAsync(request);
    };

    public postAsync = async (requestUrl: string, content: HttpContent): Promise<HttpResponseMessage> => {

        //Create the request object
        let request: HttpRequestMessage = new HttpRequestMessage();
        request.method = HttpMethods.Post;
        request.requestUri = requestUrl;
        request.content = content;

        return await this.sendAsync(request);
    };

    public putAsync = async (requestUrl: string, content: HttpContent): Promise<HttpResponseMessage> => {

        //Create the request object
        let request: HttpRequestMessage = new HttpRequestMessage();
        request.method = HttpMethods.Put;
        request.requestUri = requestUrl;
        request.content = content;

        return await this.sendAsync(request);
    };

    public sendAsync = async (request: HttpRequestMessage): Promise<HttpResponseMessage> => {

        //Run the request middleware
        HttpClient.middleware.forEach(async (value: IHttpMiddleware, key: string, map: Map<string, IHttpMiddleware>) => {
            await value.interceptRequest(request, this);
        });

        //Add the headers
        let response: Response = await fetch(this.baseAddress + request.requestUri, {
            body: request.content.toPayload(),
            headers: this.parseRequestHeaders(request),
            method: request.method,
        });

        //Package the response
        let responseMessage: HttpResponseMessage = new HttpResponseMessage(response, request);

        //Run the response middleware
        HttpClient.middleware.forEach(async (value: IHttpMiddleware, key: string, map: Map<string, IHttpMiddleware>) => {
            await value.interceptResponse(responseMessage, this);
        });

        return responseMessage;
    };

    //#endregion

    //#region Private methods


    private parseRequestHeaders = (request: HttpRequestMessage): Headers => {
        let headers: Headers = new Headers();

        //Parse the content headers
        request.content.headers.forEach((header: HttpHeader) => {
            header.values.forEach((headerValue: string) => {
                Debug.WriteLine("Name:  " + header.name);
                Debug.WriteLine("Value:  " + headerValue);
                headers.append(header.name, headerValue);
            });
        });

        //Parse the request headers
        request.headers.forEach((header: HttpHeader) => {
            header.values.forEach((headerValue: string) => {
                Debug.WriteLine("Name:  " + header.name);
                Debug.WriteLine("Value:  " + headerValue);
                headers.append(header.name, headerValue);
            });
        });

        return headers;
    }

    //#endregion


}