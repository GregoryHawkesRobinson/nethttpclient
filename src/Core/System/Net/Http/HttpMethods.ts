export enum HttpMethods {
    Get="GET",
    Head="HEAD",
    Post="Post",
    Put="PUT",
    Delete="DELETE",
    Connect="CONNECT",
    Options="Options",
    Trace="TRACE",
    Patch="PATCH"
}