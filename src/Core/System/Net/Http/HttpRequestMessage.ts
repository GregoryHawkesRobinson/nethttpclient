import {HttpContent} from "@/Core/System/Net/Http/HttpContent";
import {HttpHeaders} from "@/Core/System/Net/Http/Headers/HttpHeaders";
import {HttpMethods} from "@/Core/System/Net/Http/HttpMethods";
import {HttpRequestCache} from "@/Core/System/Net/Http/HttpRequestCache";

export class HttpRequestMessage {

    //#region Public properties

    public content: HttpContent = new HttpContent();
    public headers: HttpHeaders = new HttpHeaders();
    public method: HttpMethods = HttpMethods.Get;
    public requestUri: string = String.empty;

    //#endregion
}