import KeyValuePair from "@/Core/System/Collections/Generic/KeyValuePair";

export default class Exception extends Error{
    constructor(message?:string){
        super(message);
    }

    //#region Public properties
    public Data:KeyValuePair<string,string> = new KeyValuePair<string, string>("","");
    public HelpLink:string="";
    public HResult:number=0;
}